using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using VelocityOCR.Api.Controllers;
using VelocityOCR.Api.Controllers.Models;
using VelocityOCR.Api.Providers;
using VelocityOCR.Data;
using VelocityOCR.Data.EF;

namespace VelocityOCR.UnitTests
{
	public class VelocityOCRControllerTests
	{
		private VelocityOCRController _sut;
		private IFingerprintProvider _fingerprintProvider;
		private IUserRepository _userRepository;

		[SetUp]
		public void Setup()
		{
			var logger = Substitute.For<ILogger<VelocityOCRController>>();
			var bytesProvider = Substitute.For<IBodyBytesProvider>();
			_fingerprintProvider = Substitute.For<IFingerprintProvider>();
			_userRepository = Substitute.For<IUserRepository>();
			
			_sut = new VelocityOCRController(bytesProvider, _fingerprintProvider, _userRepository, logger);
		}

		[Test]
		public void ToText_should_validate_apiKey()
		{
			_fingerprintProvider.Fingerprint(Arg.Any<byte[]>()).Returns("fingerprint");
			_userRepository.FindTextByFingerprint("fingerprint").Returns(new RecognizedFile
			{
				Fingerprint = "fingerprint",
				RecognizedText = "the text",
				MeanConfidenceLevel = 0.7f
			});

		_userRepository.GetCallsToday("key1").Returns(9);

			var expectedResponse = new ToTextResponse
			{
				Fingerprint = "fingerprint",
				ImageText = "the text",
				MeanConfidenceLevel = 0.7f
			};
			
			var result = _sut.ToText("key1");

			result.Should().BeEquivalentTo(expectedResponse);
		}
	}
}