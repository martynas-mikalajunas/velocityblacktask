using System.IO;
using Microsoft.AspNetCore.Http;

namespace VelocityOCR.Api.Providers
{
	public interface IBodyBytesProvider
	{
		byte[] ReadBody();
	}

	public class BodyBytesProvider : IBodyBytesProvider
	{
		private readonly IHttpContextAccessor _contextAccessor;

		public BodyBytesProvider(IHttpContextAccessor contextAccessor)
		{
			_contextAccessor = contextAccessor;
		}
		
		public byte[] ReadBody()
		{
			using var ms = new MemoryStream();
			_contextAccessor.HttpContext.Request.Body.CopyToAsync(ms);
			return ms.ToArray();
		}
	}
}