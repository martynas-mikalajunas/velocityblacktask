using System;
using System.IO;
using System.Security.Cryptography;

namespace VelocityOCR.Api.Providers
{
	public interface IFingerprintProvider
	{
		string Fingerprint(byte[] data);
	}

	public class FingerprintProvider : IFingerprintProvider
	{
		public string Fingerprint(byte[] data)
		{
			using var md5 = MD5.Create();
			using var ms = new MemoryStream(data);
			
			var fingerprint = md5.ComputeHash(ms);

			return BitConverter.ToString(fingerprint);
		}
	}
}