namespace VelocityOCR.Api.Controllers.Models
{
	public class ToTextResponse
	{
		public string Fingerprint { get; set; }
		public string ImageText { get; set; }
		public float MeanConfidenceLevel { get; set; }
	}
}