﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VelocityOCR.Api.Controllers.Models;
using VelocityOCR.Api.Providers;
using VelocityOCR.Data;

namespace VelocityOCR.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class VelocityOCRController : ControllerBase
	{
		private readonly IBodyBytesProvider _bodyBytesProvider;
		private readonly IFingerprintProvider _fingerprintProvider;
		private readonly IUserRepository _userRepository;
		private readonly ILogger<VelocityOCRController> _logger;

		public VelocityOCRController(
			IBodyBytesProvider bodyBytesProvider, 
			IFingerprintProvider fingerprintProvider, 
			IUserRepository userRepository,
			ILogger<VelocityOCRController> logger)
		{
			_bodyBytesProvider = bodyBytesProvider;
			_fingerprintProvider = fingerprintProvider;
			_userRepository = userRepository;
			_logger = logger;
		}

		[HttpPost("image/toText")]
		public ToTextResponse ToText([FromQuery] string apiKey)
		{
			var imageBytes = _bodyBytesProvider.ReadBody();
			var fingerprint = _fingerprintProvider.Fingerprint(imageBytes);

			var processedFile = _userRepository.FindTextByFingerprint(fingerprint);

			return new ToTextResponse
			{
				Fingerprint = processedFile.Fingerprint,
				ImageText = processedFile.RecognizedText,
				MeanConfidenceLevel = processedFile.MeanConfidenceLevel
			};
		}
	}
}