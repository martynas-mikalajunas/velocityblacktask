using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using VelocityOCR.Api;
using VelocityOCR.Api.Controllers.Models;

namespace VelocityOCR.IntegrationTests
{
	public class VelocityOCRControllerTests
	{
		private HttpClient _client;

		[OneTimeSetUp]
		public void Setup()
		{
			var builder = new WebHostBuilder();
			var configurationBuilder = new ConfigurationBuilder();
			configurationBuilder.AddJsonFile("appsettings.json");
			builder.UseConfiguration(configurationBuilder.Build());
			builder.UseStartup<Startup>();
			var server = new TestServer(builder);
			_client = server.CreateClient();
			//_client.BaseAddress = new Uri("/VelocityOCR/image/");
		}

		[OneTimeTearDown]
		public void CleanUp()
		{
			_client.Dispose();
		}

		[Test]
		public async Task ToText_returns_text()
		{
			var responseMessage = await _client.PostAsync("/VelocityOCR/image/toText",
				new StreamContent(File.OpenRead("TestData/test.png")));

			var response = await ProcessJsonResponse<ToTextResponse>(responseMessage);

			response.Should().NotBeNull();

		}
		
		private static async Task<TResult> ProcessJsonResponse<TResult>(HttpResponseMessage responseMessage)
		{
			var options = new JsonSerializerOptions
			{
				PropertyNamingPolicy = JsonNamingPolicy.CamelCase
			};
			responseMessage.EnsureSuccessStatusCode();
			var content = await responseMessage.Content.ReadAsStringAsync();
			return JsonSerializer.Deserialize<TResult>(content, options);
		}
	}
}