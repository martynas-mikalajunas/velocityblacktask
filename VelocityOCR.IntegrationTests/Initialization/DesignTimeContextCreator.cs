using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using VelocityOCR.Data.EF;

namespace VelocityOCR.IntegrationTests.Initialization
{
	public class DesignTimeContextCreator : IDesignTimeDbContextFactory<UsersContext>
	{
		public UsersContext CreateDbContext(string[] args)
		{
			var configurationBuilder = new ConfigurationBuilder();
			configurationBuilder.AddJsonFile("appsettings.json");
			var configuration = configurationBuilder.Build();

			var options = new DbContextOptionsBuilder()
				.UseNpgsql(
					configuration.GetConnectionString("PostGres"))
				.Options;
			var context = new UsersContext(options);
			return context;
		}
	}
}