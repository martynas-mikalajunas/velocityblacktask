using VelocityOCR.Data.EF;

namespace VelocityOCR.Data
{
	public interface IUserRepository
	{
		RecognizedFile FindTextByFingerprint(string fingerprint);
		int GetCallsToday(string key1);
	}
}