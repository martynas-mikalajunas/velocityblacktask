using Microsoft.EntityFrameworkCore;

namespace VelocityOCR.Data.EF
{
	public class UsersContext : DbContext
	{
		public UsersContext(DbContextOptions options) :base(options)
		{
		}
		
		public DbSet<User> Users { get; set; }
	}
}