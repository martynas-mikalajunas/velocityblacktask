namespace VelocityOCR.Data.EF
{
	public class RecognizedFile
	{
		public string Fingerprint { get; set; }
		public string RecognizedText { get; set; }
		public float MeanConfidenceLevel { get; set; }
	}
}