namespace VelocityOCR.Data.EF
{
	public class User
	{
		public int Id { get; set; }
		public string ApiKey { get; set; }
		public int TotalDayUsage { get; set; }
	}
}