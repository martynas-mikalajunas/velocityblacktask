﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VelocityOCR.Data.EF;

namespace VelocityOCR.Data
{
	public static class Di
	{
		public static IServiceCollection AddDatabase(this IServiceCollection serviceCollection,
			IConfiguration configuration)
		{
			serviceCollection
				.AddScoped<IUserRepository, UserRepository>()
				//.AddEntityFrameworkNpgsql()
				.AddDbContext<UsersContext>(opt =>
					opt.UseNpgsql(configuration.GetConnectionString("PostGres")));

			return serviceCollection;
		}
	}
}